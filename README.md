
Converts Nintendo DS roms into installable Debian packages with proper desktop menu entries (.desktop files).

Basic example:

```
$ ls
Scribblenauts
$ nds2deb Scribblenauts.nds
$ ls 
Scribblenauts.nds
scribblenauts_1.0-1
scribblenauts_1.0-1.deb
$ sudo dpkg -i scribblenauts_1.0-1.deb
$ scribblenauts                           #starts the game in desmume
```

The package name is inferred from the name of the file, but can be 
specified with a flag, along with other metadata

```
$ nds2dev Scribblenauts.nds -t Scribblenauts -d "That one game where you can summon things by naming them" -v 2.0-1
```

Icons can be specified with a path to a local file...

```
$ nds2dev Scribblenauts.nds -i scribblenauts.png
```

...or a remote file

```
$ nds2dev Scribblenauts.nds -i http://example.org/scribblenauts.png
```



